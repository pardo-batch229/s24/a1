// Exponent Operator
let getCube = 2;
// Template Literals
console.log(`The cube of ${getCube} is ${Math.pow(getCube,3)}`);
// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];
let [array1,array2,array3,array4] = address;
console.log(`I live at ${array1} ${array2}, ${array3} ${array4}`);
// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}
let {name,species,weight,measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}`);
// Arrow Functions
let numbers = [1, 2, 3, 4, 5];
numbers.forEach((number)=>{
    console.log(number);
});

// Javascript Classes
class Dog {
    constructor(name,age,breed){
        this.name = name;
        this.age = age;
        this.breed = breed;
    }
}

let dog1 = new Dog("Frankie",5,"Miniature Dachshund");
console.log(dog1);
let dog2 = new Dog("Jamies",2,"Siberian Husky");
console.log(dog2);
